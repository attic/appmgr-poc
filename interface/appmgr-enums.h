/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __APPMGR_ENUMS__
#define __APPMGR_ENUMS__

/**
 * AppmgrApplicationState:
 * @APP_STATE_START: Give descri
 * @APP_STATE_BACKGROUND: Give description
 * @APP_STATE_SHOW: Description
 * @APP_STATE_RESTART: Description
 * @APP_STATE_OFF: Description
 * @APP_STATE_PAUSE: Description
 *
 * Application state.
 */
typedef enum
{
	APP_STATE_START = 0,
	APP_STATE_BACKGROUND,
	APP_STATE_SHOW,
	APP_STATE_RESTART,
	APP_STATE_OFF,
	APP_STATE_PAUSE
} AppmgrApplicationState ;

#endif
