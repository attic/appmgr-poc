/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <glib/gstdio.h>
#include <gio/gio.h>
#include <stdlib.h>
#include "org.apertis.Appmgr.PrivilegedApp.h"
#include "org.apertis.Appmgr.StoreApp.h"

static AppmgrPrivilegedApp *privileged_app_proxy = NULL;
static AppmgrStoreApp *store_app_proxy = NULL;

static void
test_appmgr_get_installed_applications (void);

static void
test_appmgr_get_application_info (void);
static void
test_appmgr_open_uri (void);

static void
test_appmgr_launch_application (void);

static void
test_appmgr_open_uri (void);

gint
main (gint argc, gchar *argv[])
{
  int ret;
  GError *error = NULL;
  g_test_init (&argc, &argv, NULL);

  privileged_app_proxy = appmgr_privileged_app_proxy_new_for_bus_sync ( G_BUS_TYPE_SESSION, G_BUS_NAME_WATCHER_FLAGS_NONE,
						"org.apertis.Appmgr","/org/apertis/Appmgr/PrivilegedApp",
						NULL,&error);
  store_app_proxy = appmgr_store_app_proxy_new_for_bus_sync( G_BUS_TYPE_SESSION, G_BUS_NAME_WATCHER_FLAGS_NONE,
						"org.apertis.Appmgr","/org/apertis/Appmgr/StoreApp",
						NULL,&error);

  g_test_add_func ("/appmgr-tests/test_appmgr_get_installed_applications",
		test_appmgr_get_installed_applications);
  g_test_add_func ("/appmgr-tests/test_appmgr_get_application_info",
		test_appmgr_get_application_info);
  g_test_add_func ("/appmgr-tests/test_appmgr_open_uri",
		  test_appmgr_open_uri);
  g_test_add_func ("/appmgr-tests/test_appmgr_launch_application",
		test_appmgr_launch_application);
  
  ret = g_test_run ();

  return ret;
}

static void
test_appmgr_get_application_info (void)
{
	GVariant *applicationInfo = NULL;
	GVariantIter iter;
	GError *error = NULL;
	gchar *value = NULL;
	gchar *key = NULL;
	gboolean result;
	result = appmgr_privileged_app_call_get_application_info_sync (privileged_app_proxy,
						"org.apertis.Eye.desktop",&applicationInfo,
						NULL,&error);
	g_assert_no_error(error);
	g_assert(result);
	g_variant_iter_init (&iter, applicationInfo);
	while(g_variant_iter_next(&iter, "{ss}", &key,&value))
	{
		g_debug("%s : %s",key,value);
	}
}

static void
test_appmgr_get_installed_applications (void)
{
	gchar **applicationList = NULL;
	GError *error = NULL;
	gboolean result;
	result = appmgr_privileged_app_call_get_installed_applications_sync(privileged_app_proxy,
						&applicationList,NULL,&error);
	g_assert_no_error(error);
	g_assert(result);
	for( ; applicationList!=NULL && *applicationList!=NULL ; applicationList++)
        {
		g_debug("Apertis Applications = %s", *applicationList);
	}
}

static void
test_appmgr_launch_application (void)
{
	GError *error = NULL;
	gboolean result = appmgr_privileged_app_call_launch_application_sync ( privileged_app_proxy,"org.apertis.Eye.desktop",NULL,&error);
	g_assert_no_error(error);
	g_assert(result);
}
static void test_appmgr_open_uri (void)
{
	gboolean result;
	GError *error = NULL;
	gchar **app_for_mime = NULL;
	result = appmgr_store_app_call_open_uri_sync(store_app_proxy,"http",&app_for_mime,NULL,&error);
	for( ; app_for_mime!=NULL && *app_for_mime!=NULL ; app_for_mime++)
	{
	   	g_print("handles http URI = %s \n", *app_for_mime);
	}
		
	g_assert_no_error(error);
	g_assert(result);
	
}

