/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __PRIVILEGED_APP_HELPER_H__
#define __PRIVILEGED_APP_HELPER_H__

#include <glib-object.h>
#include <gio/gdesktopappinfo.h>
#include <gio/gio.h>
#include "appmgr-internal.h"

G_BEGIN_DECLS

#define PRIVILEGED_TYPE_APP_HELPER privileged_app_helper_get_type()

#define PRIVILEGED_APP_HELPER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  PRIVILEGED_TYPE_APP_HELPER, PrivilegedAppHelper))

#define PRIVILEGED_APP_HELPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  PRIVILEGED_TYPE_APP_HELPER, PrivilegedAppHelperClass))

#define PRIVILEGED_IS_APP_HELPER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  PRIVILEGED_TYPE_APP_HELPER))

#define PRIVILEGED_IS_APP_HELPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  PRIVILEGED_TYPE_APP_HELPER))

#define PRIVILEGED_APP_HELPER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  PRIVILEGED_TYPE_APP_HELPER, PrivilegedAppHelperClass))

typedef struct _PrivilegedAppHelper PrivilegedAppHelper;
typedef struct _PrivilegedAppHelperClass PrivilegedAppHelperClass;
typedef struct _PrivilegedAppHelperPrivate PrivilegedAppHelperPrivate;

struct _PrivilegedAppHelper
{
  GObject parent;

  PrivilegedAppHelperPrivate *priv;
};

struct _PrivilegedAppHelperClass
{
  GObjectClass parent_class;
};

GType privileged_app_helper_get_type (void) G_GNUC_CONST;

PrivilegedAppHelper *privileged_app_helper_new (void);

void init_privileged_app_helper(GDBusConnection *connection);

gchar ** get_app_list(void);
GVariant * get_desktop_app_info(gchar *pDesktopId);

G_END_DECLS

#endif /* __PRIVILEGED_APP_HELPER_H__ */
