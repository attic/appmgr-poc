/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "privileged-app-helper.h"
G_DEFINE_TYPE (PrivilegedAppHelper, privileged_app_helper, G_TYPE_OBJECT)

#define APP_HELPER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), PRIVILEGED_TYPE_APP_HELPER, PrivilegedAppHelperPrivate))

AppmgrPrivilegedApp *pPrivilegedApp = NULL;
static gboolean handle_get_application_info_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
                                gchar *argApplicationId,
                                gpointer pUserData );
static gboolean handle_get_installed_applications_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
                                gpointer pUserData );
static gboolean handle_launch_application_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
				gchar *argApplicationId,
                                gpointer pUserData );

static void
file_monitor_change_event_clb (GFileMonitor     *monitor,
               GFile            *file,
               GFile            *other_file,
               GFileMonitorEvent event_type,
               gpointer          user_data);

static gboolean is_an_apertis_application(GAppInfo *app_info);

struct _PrivilegedAppHelperPrivate
{
};


static void
privileged_app_helper_get_property (GObject    *object,
                                    guint       property_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
privileged_app_helper_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
privileged_app_helper_dispose (GObject *object)
{
  G_OBJECT_CLASS (privileged_app_helper_parent_class)->dispose (object);
}

static void
privileged_app_helper_finalize (GObject *object)
{
  G_OBJECT_CLASS (privileged_app_helper_parent_class)->finalize (object);
}

static void
privileged_app_helper_class_init (PrivilegedAppHelperClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (PrivilegedAppHelperPrivate));

  object_class->get_property = privileged_app_helper_get_property;
  object_class->set_property = privileged_app_helper_set_property;
  object_class->dispose = privileged_app_helper_dispose;
  object_class->finalize = privileged_app_helper_finalize;
}

static void
privileged_app_helper_init (PrivilegedAppHelper *self)
{
  self->priv = APP_HELPER_PRIVATE (self);
}

PrivilegedAppHelper *
privileged_app_helper_new (void)
{
  return g_object_new (PRIVILEGED_TYPE_APP_HELPER, NULL);
}

static void
file_monitor_change_event_clb (GFileMonitor     *monitor,
               GFile            *file,
               GFile            *other_file,
               GFileMonitorEvent event_type,
               gpointer          user_data)
{
	if(monitor !=NULL)
	{
		GDesktopAppInfo *pDesktopApp = g_desktop_app_info_new(g_file_get_basename(file));
		g_debug("Base file name = %s",g_file_get_basename(file));
		if(pDesktopApp)
		{
			if((g_desktop_app_info_has_key(pDesktopApp,"X-Apertis-Type")))
			{
				if(!(g_strcmp0((g_desktop_app_info_get_string(pDesktopApp,"X-Apertis-Type")),"application")))
				{
					if(event_type == G_FILE_MONITOR_EVENT_CREATED)
					{
						appmgr_privileged_app_emit_application_info_updated(pPrivilegedApp);
					}
					#if 0
					g_debug("Apertis application = %s", g_file_get_basename(file));
					if(event_type == G_FILE_MONITOR_EVENT_DELETED)
					{
						g_debug("Looks like %s application is uninstalled",g_file_get_basename(file));
					}
					else if(event_type == G_FILE_MONITOR_EVENT_ATTRIBUTE_CHANGED)
					{
						g_debug("Looks like application attribute changed");
					}
					#endif
				}
				else
					return;
			}
			else
				g_debug("Not an apertis application");
		}
		else if(event_type == G_FILE_MONITOR_EVENT_DELETED)
		{
			appmgr_privileged_app_emit_application_info_updated(pPrivilegedApp);
		}
	}
}

/*********************************************************************************************
 * Function: init_privileged_app_interface
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
void init_privileged_app_interface(GDBusConnection *connection)
{
	GFile *entryPointDir = g_file_new_for_path(g_build_filename("/usr","share","applications",NULL));

	pPrivilegedApp = appmgr_privileged_app_skeleton_new();
	g_signal_connect (pPrivilegedApp,
				"handle-get-application-info",
                            	G_CALLBACK (handle_get_application_info_clb),
                            	NULL);

	g_signal_connect (pPrivilegedApp,
        			"handle-get-installed-applications",
                            	G_CALLBACK (handle_get_installed_applications_clb),
                            	NULL);

	g_signal_connect (pPrivilegedApp,
        			"handle-launch-application",
                            	G_CALLBACK (handle_launch_application_clb),
                            	NULL);

	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pPrivilegedApp),
						connection,
                                                "/org/apertis/Appmgr/PrivilegedApp",
                                                NULL))
	{
		g_debug("export error");

	}

	if(entryPointDir)
	{
		GError *error = NULL;
		GFileMonitor *entryPointMonitor = g_file_monitor_directory(entryPointDir,G_FILE_MONITOR_WATCH_MOVES,NULL,&error);
		if(entryPointMonitor != NULL)
		{
			g_signal_connect (entryPointMonitor, "changed",G_CALLBACK (file_monitor_change_event_clb),NULL);
		}
		else
		{
			g_debug("Error monitoring file = %s",error->message);
		}
	}
}

static gboolean is_an_apertis_application(GAppInfo *app_info)
{
	gchar **argv;
	GError **error = NULL;
	if(!g_shell_parse_argv(g_app_info_get_commandline(app_info),NULL,&argv,error))
	{
		g_debug("Not an Apertis application");
		return FALSE;
	}
	else if(argv == NULL || argv[0] == NULL)
	{
		g_strfreev(argv);
		g_set_error(error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
		"No Exec= command-line found");
		return FALSE;
	}
	else
	{
		GDesktopAppInfo *desktop_app = G_DESKTOP_APP_INFO(app_info);
		if((g_desktop_app_info_has_key(desktop_app,"X-Apertis-Type")))
		{
			if(!(g_strcmp0((g_desktop_app_info_get_string(desktop_app,"X-Apertis-Type")),"application")))
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}

/*********************************************************************************************
 * Function:    handle_app_info_clb
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
gchar ** get_app_list(void)
{
	GList *pApplicationList = NULL;
	gchar **pAppList = g_new0(gchar *,1);
	guint index = 0;
	pApplicationList = g_app_info_get_all ();
	for( ; pApplicationList != NULL ; pApplicationList = pApplicationList->next)
	{
		if(is_an_apertis_application(pApplicationList->data))
		{
			g_debug("Apertis applications = %s", g_app_info_get_id(pApplicationList->data));
			pAppList = g_renew(gchar *, pAppList, index+2);
			pAppList[index] = g_strdup(g_app_info_get_id(pApplicationList->data));
			index++;
		}
	}
	pAppList[index] = NULL;
	return pAppList;
}

/*********************************************************************************************
 * Function:    handle_app_info_clb
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
static gboolean handle_get_installed_applications_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
                                gpointer pUserData )
{
	gchar **pAppList = NULL;
	pAppList = get_app_list();
	appmgr_privileged_app_complete_get_installed_applications(object,invocation,(const gchar **) pAppList);
	return TRUE;
}

/*********************************************************************************************
 * Function:    handle_app_info_clb
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
GVariant * get_desktop_app_info(gchar *pDesktopId)
{
	GDesktopAppInfo *pDesktopApp = g_desktop_app_info_new((const gchar *)pDesktopId);
	GVariant *pAppInfo = NULL;
	GVariantBuilder *pAppInfoBuilder = NULL;
	pAppInfoBuilder = g_variant_builder_new(G_VARIANT_TYPE ("a{ss}"));
	g_debug("pDesktopId = %s ",pDesktopId);
	if(pDesktopApp)
	{
		GError *pError = NULL;
		GKeyFile *pKeyFile = g_key_file_new();
		if((g_key_file_load_from_file(pKeyFile,g_desktop_app_info_get_filename(pDesktopApp),G_KEY_FILE_NONE,&pError)))
		{
			if(g_key_file_has_group(pKeyFile,(const gchar *)"Desktop Entry"))
			{
				gchar **keys = g_key_file_get_keys(pKeyFile,(const gchar *)"Desktop Entry",
								NULL,&pError);
				if(keys != NULL)
				{
					for( ; keys!=NULL && *keys!=NULL ; keys++)
					{
						gchar *value = g_key_file_get_value(pKeyFile,"Desktop Entry",*keys,&pError);
						g_variant_builder_add(pAppInfoBuilder, "{ss}",*keys,value);
						g_debug("Key and value = %s %s", *keys,value);
						g_free(value);
					}
					pAppInfo = g_variant_builder_end(pAppInfoBuilder);
				}
			}
		}
		else
			g_debug("Not able to load key file %s",pError->message);
		g_key_file_free(pKeyFile);
	}
	return pAppInfo;
}

/*********************************************************************************************
 * Function:    handle_app_info_clb
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
static gboolean handle_get_application_info_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
				gchar *argApplicationId,
                                gpointer pUserData )
{
	GVariant *pAppInfo = get_desktop_app_info(argApplicationId);
	appmgr_privileged_app_complete_get_application_info(object,invocation,pAppInfo);
	return TRUE;
}

/*********************************************************************************************
 * Function:    handle_app_info_clb
 * Description:
 * Parameters:  None
 * Return:      None
 ********************************************************************************************/
static gboolean handle_launch_application_clb( AppmgrPrivilegedApp *object,
                                GDBusMethodInvocation   *invocation,
				gchar *argApplicationId,
                                gpointer pUserData )
{
	GDesktopAppInfo *desktop_app = g_desktop_app_info_new((const gchar *)argApplicationId);
	if(desktop_app)
	{
		if(g_desktop_app_info_has_key(desktop_app,"Exec"))
		{
			gchar *exec_path = g_desktop_app_info_get_string(desktop_app,"Exec");
			gchar **cmd_line_params = g_strsplit(exec_path," ",0);
			gchar **tmp = cmd_line_params;
			gint index = 0;
			for(; tmp != NULL && *tmp != NULL ; tmp++)
			{
				index++;
				g_debug("Split Args = %s",*tmp);
			}
			appmgr_privileged_app_complete_launch_application(object,invocation);
			init_exec(index,cmd_line_params);
		}
	}
	else
	{
		g_critical(" %s application not found",argApplicationId);
		return FALSE;
	}
	return TRUE;
}
