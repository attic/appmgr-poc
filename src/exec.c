/*
 * canterbury-exec: execute a program in an app-bundle
 *
 * Copyright © 2015-2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>
#include <glib/gstdio.h>
#include <systemd/sd-journal.h>

#include <canterbury/canterbury.h>
#include <canterbury/canterbury-platform.h>

static GOptionEntry entries[] = { { NULL } };

/* consistent with nice(1), etc. */
enum
{
  CBY_EXEC_STATUS_FAILED = 125,
  CBY_EXEC_STATUS_NOT_EXECUTED = 126,
  CBY_EXEC_STATUS_NOT_FOUND = 127
};

/*
 * If @variable is in canterbury-exec's environment, copy it into @envp.
 *
 * Returns: the new value of envp
 */
static gchar **
inherit_environment (gchar **envp, const gchar *variable)
{
  const gchar *value;

  value = g_getenv (variable);

  if (value != NULL)
    envp = g_environ_setenv (envp, variable, value, TRUE);

  return envp;
}

/*
 * Create a subdirectory of @path named @subdir with permissions 0700
 * (owner-private), or exit if not possible.
 *
 * If @variable is not NULL, set that variable in @envp to the
 * resulting subdirectory.
 *
 * Returns: the new value of envp
 */
static gchar **
ensure_directory_or_die (gchar **envp,
                         const gchar *variable,
                         const gchar *path,
                         const gchar *subdir)
{
  g_autofree gchar *full_path = g_build_filename (path, subdir, NULL);

  if (variable != NULL)
    envp = g_environ_setenv (envp, variable, full_path, TRUE);

  if (g_mkdir_with_parents (full_path, 0700) != 0)
    {
      int saved_errno = errno;

      g_printerr ("%s: unable to create \"%s\": %s\n", g_get_prgname (),
                  full_path, g_strerror (saved_errno));
      exit (CBY_EXEC_STATUS_FAILED);
    }

  return envp;
}

/*
 * Redirect stdio file @file (described as @description in error messages)
 * to the Journal, with priority @priority and syslog tag @bundle_id.
 * If we cannot, exit.
 */
static void
redirect_or_die (const gchar *bundle_id,
                 int priority,
                 const gchar *description,
                 FILE *file)
{
  int journal_fd;
  GError *error = NULL;

  journal_fd = sd_journal_stream_fd (bundle_id, priority, FALSE);

  if (journal_fd < 0)
    {
      g_printerr ("%s: unable to open Journal stream for %s: %s\n",
                  g_get_prgname (), description, g_strerror (-journal_fd));
      exit (CBY_EXEC_STATUS_FAILED);
    }

  if (fflush (file) != 0)
    {
      g_printerr ("%s: unable to flush %s: %s\n",
                  g_get_prgname (), description, g_strerror (errno));
      exit (CBY_EXEC_STATUS_FAILED);
    }

  if (dup2 (journal_fd, fileno (file)) < 0)
    {
      g_printerr ("%s: unable to redirect %s to Journal stream: %s\n",
                  g_get_prgname (), description, g_strerror (errno));
      exit (CBY_EXEC_STATUS_FAILED);
    }

  if (!g_close (journal_fd, &error))
    {
      g_printerr ("%s: unable to close Journal stream: %s\n", g_get_prgname (),
                  error->message);
      g_error_free (error);
      exit (CBY_EXEC_STATUS_FAILED);
    }
}

static void
check_close_on_exec (void)
{
  GError *error = NULL;
  g_autoptr (GDir) dir_iter = g_dir_open ("/proc/self/fd", 0, &error);

  if (dir_iter == NULL)
    {
      g_printerr ("%s: unable to iterate over file descriptors: %s\n",
                  g_get_prgname (), error->message);
      g_error_free (error);
      /* not fatal - all fds from libraries should be close-on-exec anyway */
      return;
    }

  while (1)
    {
      const gchar *dir_entry = g_dir_read_name (dir_iter);
      gchar *non_integer = NULL;
      /* fds are ints, but we use a gint64 for g_ascii_strtoll(), and
       * range-check it explicitly. */
      gint64 fd;

      if (dir_entry == NULL)
        break;

      fd = g_ascii_strtoll (dir_entry, &non_integer, 10);

      /* Skip non-integers (*non_integer would not be \0), negative or
       * otherwise out-of-range integers, and fds 0, 1, 2 (stdin, stdout,
       * stderr, which should not be close-on-exec) */
      if (non_integer != NULL && *non_integer == '\0' && fd > 2 &&
          fd <= INT_MAX)
        {
          int flags = fcntl (fd, F_GETFD, 0);

          if (flags >= 0 && (flags & FD_CLOEXEC) == 0)
            {
              g_autofree gchar *path = g_build_filename ("/proc/self/fd",
                                                         dir_entry, NULL);
              gchar buf[64] = { 0 };

              /* If it's very long, it's OK to truncate: we only want to
               * show a reasonable prefix of it anyway. */
              if (readlink (path, buf, sizeof (buf)) > 0)
                {
                  buf[sizeof (buf) - 1] = '\0';
                  g_warning ("inherited fd %" G_GINT64_FORMAT " \"%s\" was not "
                           "close-on-exec", fd, buf);
                }
              else
                {
                  g_warning ("inherited fd %" G_GINT64_FORMAT " was not "
                           "close-on-exec, and readlink() failed to "
                           "identify it: %s", fd, g_strerror (errno));
                }

              if (fcntl (fd, F_SETFD, flags | FD_CLOEXEC) < 0)
                g_warning ("unable to make fd %" G_GINT64_FORMAT
                         " close-on-exec: %s", fd, g_strerror (errno));
            }
        }
    }
}

/* FIXME: Adding direct g_autoptr support to gdbus-codegen breaks some existing
 * code (see <https://bugzilla.gnome.org/show_bug.cgi?id=763379>) so do this
 * differently for now */
typedef CbyPrivilegedAppHelper1 AutoCbyPrivilegedAppHelper1;
G_DEFINE_AUTOPTR_CLEANUP_FUNC (AutoCbyPrivilegedAppHelper1, g_object_unref)

int
init_exec (int argc, char **argv)
{
  GError *error = NULL;
  g_autoptr (GOptionContext) context = NULL;
  int saved_errno;
  const gchar *bundle_id;
  const gchar *persistence_path;
  const gchar *static_prefix;
  g_autoptr (CbyProcessInfo) pi = NULL;
  CbyProcessType type;
  g_autofree gchar *ribchester_created = NULL;
  g_autofree gchar *tmp = NULL;
  g_autofree gchar *contents = NULL;
  g_auto (GStrv) envp = NULL;
  g_autoptr (AutoCbyPrivilegedAppHelper1) proxy = NULL;

  context = g_option_context_new ("- launch an application bundle");
  g_option_context_add_main_entries (context, entries, NULL);

  if (!g_option_context_parse (context, &argc, &argv, &error))
    {
      g_printerr ("%s\n", error->message);
      g_error_free (error);
      return CBY_EXEC_STATUS_FAILED;
    }

  if (argc < 2)
    {
      g_printerr ("%s: a command to execute is required\n", g_get_prgname ());
      return CBY_EXEC_STATUS_FAILED;
    }

  if (!g_path_is_absolute (argv[0]))
    {
      g_printerr ("%s: first argument must be an absolute path, not \"%s\"\n",
                  g_get_prgname (), argv[0]);
      return CBY_EXEC_STATUS_FAILED;
    }

  pi = cby_process_info_new_for_path_and_user (argv[0], getuid ());
  type = cby_process_info_get_process_type (pi);
  bundle_id = cby_process_info_get_bundle_id (pi);
  persistence_path = cby_process_info_get_persistence_path (pi);

  if ((type != CBY_PROCESS_TYPE_BUILT_IN_BUNDLE &&
       type != CBY_PROCESS_TYPE_STORE_BUNDLE) ||
      bundle_id == NULL || !cby_is_bundle_id (bundle_id))
    {
      g_printerr ("%s: first argument must point into an app-bundle, "
                  "not \"%s\"\n",
                  g_get_prgname (), argv[0]);
      return CBY_EXEC_STATUS_FAILED;
    }

  if (persistence_path == NULL)
    {
      g_printerr ("%s: unable to determine persistence path for \"%s\"\n",
                  g_get_prgname (), argv[0]);
      return CBY_EXEC_STATUS_FAILED;
    }

  /* Ask Ribchester to create the persistence path. We block here,
   * because there's no real reason not to: this is a single-purpose
   * process. */
  proxy = cby_privileged_app_helper1_proxy_new_for_bus_sync (
      G_BUS_TYPE_SYSTEM,
      /* We are only going to use this for one method call, so the
       * proxy doesn't need to be stateful. Let the actual method call
       * activate the provider. */
      (G_DBUS_PROXY_FLAGS_DO_NOT_LOAD_PROPERTIES |
       G_DBUS_PROXY_FLAGS_DO_NOT_CONNECT_SIGNALS |
       G_DBUS_PROXY_FLAGS_DO_NOT_AUTO_START_AT_CONSTRUCTION),
      "org.apertis.Ribchester", "/org/apertis/Canterbury/PrivilegedAppHelper1",
      NULL, &error);

  if (proxy == NULL)
    {
      g_printerr ("%s: %s\n", g_get_prgname (), error->message);
      g_error_free (error);
      return CBY_EXEC_STATUS_FAILED;
    }

  if (!cby_privileged_app_helper1_call_prepare_app_bundle_sync (
          proxy, bundle_id, CBY_PREPARE_APP_BUNDLE_FLAGS_NONE,
          &ribchester_created, NULL, &error))
    {
      g_printerr ("%s: %s\n", g_get_prgname (), error->message);
      g_error_free (error);
      return CBY_EXEC_STATUS_FAILED;
    }

  /* No need for this any more */
  g_clear_object (&proxy);

  if (strcmp (ribchester_created, persistence_path) != 0)
    {
      g_warning ("We expected \"%s\" but Ribchester created \"%s\"",
               persistence_path, ribchester_created);
      persistence_path = ribchester_created;
    }

  /* Start with an empty environment */
  envp = NULL;
  /* Inherit the basics */
  envp = inherit_environment (envp, "DISPLAY");
  envp = inherit_environment (envp, "HOME");
  envp = inherit_environment (envp, "LANG");
  envp = inherit_environment (envp, "LOGNAME");
  envp = inherit_environment (envp, "SHELL");
  envp = inherit_environment (envp, "USER");
  envp = inherit_environment (envp, "USERNAME");
  envp = inherit_environment (envp, "XDG_RUNTIME_DIR");
  /* Hard-coded values */
  envp = g_environ_setenv (envp, "XDG_CURRENT_DESKTOP", "Apertis", TRUE);

  /* See https://wiki.apertis.org/Application_Layout for details of the
   * directories used here.*/

  /* Created in the persistence path */
  envp = ensure_directory_or_die (envp, "XDG_CACHE_HOME", persistence_path,
                                  "cache");
  envp = ensure_directory_or_die (envp, "XDG_CONFIG_HOME", persistence_path,
                                  "config");
  envp = ensure_directory_or_die (envp, "XDG_DATA_HOME", persistence_path,
                                  "data");

  /* Created in the persistence path, but not in the environment */
  ensure_directory_or_die (NULL, NULL, persistence_path, "downloads");

  if (type == CBY_PROCESS_TYPE_BUILT_IN_BUNDLE)
    static_prefix = CBY_PATH_PREFIX_BUILT_IN_BUNDLE;
  else if (type == CBY_PROCESS_TYPE_STORE_BUNDLE)
    static_prefix = CBY_PATH_PREFIX_STORE_BUNDLE;
  else
    g_return_val_if_reached (CBY_EXEC_STATUS_FAILED);

  tmp = g_strdup_printf ("%s/%s/share:"
                         "/var/lib/apertis_extensions/public:"
                         "/usr/share",
                         static_prefix, bundle_id);
  envp = g_environ_setenv (envp, "XDG_DATA_DIRS", tmp, TRUE);
  g_clear_pointer (&tmp, g_free);

  tmp = g_strdup_printf (CBY_PATH_PREFIX_VARIABLE_DATA "/%s/etc/xdg:"
                         "%s/%s/etc/xdg:"
                         "/etc/xdg",
                         bundle_id, static_prefix, bundle_id);
  envp = g_environ_setenv (envp, "XDG_CONFIG_DIRS", tmp, TRUE);
  g_clear_pointer (&tmp, g_free);

  tmp = g_strdup_printf ("%s/%s/bin:/usr/bin:/bin", static_prefix, bundle_id);
  envp = g_environ_setenv (envp, "PATH", tmp, TRUE);
  g_clear_pointer (&tmp, g_free);

  tmp = g_strdup_printf ("%s/config/user-dirs.dirs", persistence_path);
  contents = g_strdup_printf (
      /* downloads are in the per-(app,user) directory */
      "XDG_DOWNLOAD_DIR=\"%s/downloads\"\n"
      /* public shared content is in /home/shared */
      "XDG_PUBLICSHARE_DIR=\"/home/shared\"\n"
      "XDG_MUSIC_DIR=\"/home/shared/Music\"\n"
      "XDG_PICTURES_DIR=\"/home/shared/Pictures\"\n"
      "XDG_VIDEOS_DIR=\"/home/shared/Videos\"\n"
      /* FIXME: do we want these somewhere else? This is an unresolved
       * design question on <https://wiki.apertis.org/Application_Layout>.
       * "$HOME/" is the standard way to "disable" certain special
       * directories. */
      "XDG_DESKTOP_DIR=\"$HOME/\"\n"
      "XDG_TEMPLATES_DIR=\"$HOME/\"\n"
      "XDG_DOCUMENTS_DIR=\"$HOME/\"\n",
      persistence_path);

  if (!g_file_set_contents (tmp, contents, -1, &error))
    {
      g_printerr ("%s: unable to configure user directories in \"%s\": %s\n",
                  g_get_prgname (), tmp, error->message);
      g_error_free (error);
      return CBY_EXEC_STATUS_FAILED;
    }

  g_clear_pointer (&contents, g_free);
  g_clear_pointer (&tmp, g_free);

  redirect_or_die (bundle_id, LOG_INFO, "stdout", stdout);
  redirect_or_die (bundle_id, LOG_WARNING, "stderr", stderr);

  /* we have to leak envp under normal circumstances, because we pass it to
   * execve - but our memory gets cleared by the exec anyway */

  check_close_on_exec ();

  g_assert (argv[argc] == NULL);
  execve (argv[0], argv + 1, envp);
  saved_errno = errno;
  g_printerr ("%s: unable to execute \"%s\": %s\n", g_get_prgname (), argv[1],
              g_strerror (saved_errno));

  if (saved_errno == ENOENT)
    return CBY_EXEC_STATUS_NOT_FOUND;

  return CBY_EXEC_STATUS_NOT_EXECUTED;
}
