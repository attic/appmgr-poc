/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "appmgr-internal.h"

static void
on_bus_acquired(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);
static void
on_name_acquired(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);

static void
on_name_lost(GDBusConnection *pConnection, const gchar *pName,
		gpointer pUserData);

static void
on_bus_acquired (GDBusConnection *pConnection, const gchar *pName,
                 gpointer pUserData)
{
	init_privileged_app_interface(pConnection);
	init_store_app_interface(pConnection);
}

static void
on_name_acquired (GDBusConnection *pConnection, const gchar *pName,
                  gpointer pUserData)
{
}

static void
on_name_lost (GDBusConnection *pConnection, const gchar *pName,
              gpointer pUserData)
{
}

gint main(gint argc, gchar *argv[])
{

	GMainLoop *pMainLoop = NULL;

	g_setenv ("XDG_DATA_DIRS",
			"/var/lib/apertis_extensions:"
			"/var/lib/MILDENHALL_extensions:"
			"/usr/local/share:/usr/share", TRUE);

	guint owner_id;

	owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,           // bus type
			"org.apertis.Appmgr",       // interface pName
			G_BUS_NAME_OWNER_FLAGS_NONE, // bus own flag, can be used to take away the bus and give it to another service
			on_bus_acquired,        // callback invoked when the bus is acquired
			on_name_acquired, // callback invoked when interface pName is acquired
			on_name_lost, // callback invoked when pName is lost to another service or other reason
			NULL,                         // user data
			NULL);                        // user data free func

	pMainLoop = g_main_loop_new(NULL, FALSE);

	g_main_loop_run(pMainLoop);
	g_bus_unown_name(owner_id);
	exit(0);
}
