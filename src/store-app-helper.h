/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __STORE_APP_HELPER_H__
#define __STORE_APP_HELPER_H__

#include <glib-object.h>
#include <gio/gdesktopappinfo.h>
#include "appmgr-internal.h"

G_BEGIN_DECLS

#define STORE_TYPE_APP_HELPER store_app_helper_get_type()

#define STORE_APP_HELPER(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
  STORE_TYPE_APP_HELPER, StoreAppHelper))

#define STORE_APP_HELPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), \
  STORE_TYPE_APP_HELPER, StoreAppHelperClass))

#define STORE_IS_APP_HELPER(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), \
  STORE_TYPE_APP_HELPER))

#define STORE_IS_APP_HELPER_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), \
  STORE_TYPE_APP_HELPER))

#define STORE_APP_HELPER_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), \
  STORE_TYPE_APP_HELPER, StoreAppHelperClass))

typedef struct _StoreAppHelper StoreAppHelper;
typedef struct _StoreAppHelperClass StoreAppHelperClass;
typedef struct _StoreAppHelperPrivate StoreAppHelperPrivate;

struct _StoreAppHelper
{
  GObject parent;

  StoreAppHelperPrivate *priv;
};

struct _StoreAppHelperClass
{
  GObjectClass parent_class;
};

GType store_app_helper_get_type (void) G_GNUC_CONST;

StoreAppHelper *store_app_helper_new (void);

G_END_DECLS

#endif /* __STORE_APP_HELPER_H__ */
