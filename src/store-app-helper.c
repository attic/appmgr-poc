/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "store-app-helper.h"

G_DEFINE_TYPE (StoreAppHelper, store_app_helper, G_TYPE_OBJECT)

#define APP_HELPER_PRIVATE(o) \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), STORE_TYPE_APP_HELPER, StoreAppHelperPrivate))

struct _StoreAppHelperPrivate
{
};

AppmgrStoreApp *pStoreApp = NULL;

static gboolean handle_open_uri_clb( AppmgrStoreApp *object,GDBusMethodInvocation   *invocation,
					const gchar *mime,
                                        gpointer pUserData );

static gchar** get_apps_for_uri(const gchar *uri_scheme);

static void
store_app_helper_get_property (GObject    *object,
                               guint       property_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
store_app_helper_set_property (GObject      *object,
                               guint         property_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  switch (property_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
store_app_helper_dispose (GObject *object)
{
  G_OBJECT_CLASS (store_app_helper_parent_class)->dispose (object);
}

static void
store_app_helper_finalize (GObject *object)
{
  G_OBJECT_CLASS (store_app_helper_parent_class)->finalize (object);
}

static void
store_app_helper_class_init (StoreAppHelperClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  g_type_class_add_private (klass, sizeof (StoreAppHelperPrivate));

  object_class->get_property = store_app_helper_get_property;
  object_class->set_property = store_app_helper_set_property;
  object_class->dispose = store_app_helper_dispose;
  object_class->finalize = store_app_helper_finalize;
}

static void
store_app_helper_init (StoreAppHelper *self)
{
  self->priv = APP_HELPER_PRIVATE (self);
}

StoreAppHelper *
store_app_helper_new (void)
{
  return g_object_new (STORE_TYPE_APP_HELPER, NULL);
}

static gboolean handle_open_uri_clb( AppmgrStoreApp *object,
				     GDBusMethodInvocation *invocation,
				     const gchar *uri,
                     gpointer pUserData )
{

	gchar ** app_id = NULL;
	app_id = get_apps_for_uri (uri);
	appmgr_store_app_complete_open_uri(object,invocation,(const gchar* const *) app_id);
	return TRUE;
}


void init_store_app_interface(GDBusConnection *pConnection)
{

	pStoreApp = appmgr_store_app_skeleton_new();
	
	g_signal_connect (pStoreApp,
                                "handle-open-uri",
                                G_CALLBACK (handle_open_uri_clb),
                                NULL);
	if (!g_dbus_interface_skeleton_export (G_DBUS_INTERFACE_SKELETON (pStoreApp),
                                                pConnection,
                                                "/org/apertis/Appmgr/StoreApp",
                                                NULL))
        {
                g_debug("export error \n");

        }

}

static gchar** get_apps_for_uri(const gchar *uri_scheme)
{
    GList *pApplicationList = NULL;
    gchar **pAppList = g_new0(gchar *,1);
    guint index = 0;
    pApplicationList = g_app_info_get_all ();
    for( ; pApplicationList != NULL ; pApplicationList = pApplicationList->next)
    {
            gchar **argv;
            GError **error = NULL;
            if (!g_shell_parse_argv (g_app_info_get_commandline (pApplicationList->data),
                    NULL, &argv, error))
            {
                    g_print("Not an Apertis application");
                    break;
            }
            else if (argv == NULL || argv[0] == NULL)
            {
                    g_strfreev (argv);
                    g_set_error (error, G_IO_ERROR, G_IO_ERROR_INVALID_ARGUMENT,
                    "No Exec= command-line found");
                    break;
            }
            else
            {
              if((G_IS_DESKTOP_APP_INFO(pApplicationList->data)))
              {
                   GDesktopAppInfo *pDesktop = G_DESKTOP_APP_INFO(pApplicationList->data);
                   if (g_desktop_app_info_has_key (pDesktop, "X-Apertis-Type") &&
                       g_desktop_app_info_has_key (pDesktop, "MimeType"))
                   {
						gchar *appMIME = g_strrstr (g_desktop_app_info_get_string(pDesktop,"MimeType"),uri_scheme);
						if(NULL == appMIME)
						{
							//This app does not support the required mime...move on
							
						}	
						else
						{
							g_debug("App found that supports URI  - %s", g_app_info_get_id(pApplicationList->data));
							pAppList = g_renew(gchar *, pAppList, index+2);
							pAppList[index] = g_strdup(g_app_info_get_id(pApplicationList->data));
							index++;
		
						}
					}
              }
            }
            
    	}
    	pAppList[index] = NULL;
		return pAppList;
}
